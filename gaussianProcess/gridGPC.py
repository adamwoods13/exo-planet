# -*- coding: utf-8 -*-
"""
Created on Sat Jan  2 14:35:22 2021

@author: Adam Woods
"""
import pandas as pd
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import RBF
from sklearn.gaussian_process.kernels import DotProduct
from sklearn.gaussian_process.kernels import Matern
from sklearn.gaussian_process.kernels import RationalQuadratic
from sklearn.model_selection import train_test_split
from sklearn.model_selection import GridSearchCV

#Sets seed
import numpy as np
import random as rn
s = 17 #17 91.30
np.random.seed(s)
rn.seed(s)

#loads data sets
df = pd.read_csv('../data/local_data.csv', sep=',', header=None)

#Splits and shuffles the data into test and train
df = df.sample(frac=1).reset_index(drop=True)
df_train, df_test = train_test_split(df, test_size=0.1)

#Splits training and testing data into labels and features as well as other information
x_train = df_train.drop([0, 1, 2], axis=1)
y_train = df_train[2]
id_train = df_train[0]
label_train = df_train[1]
x_test = df_test.drop([0, 1, 2], axis=1)
y_test = df_test[2]
id_test = df_test[0]
label_test = df_test[1]

#Defines kernels
dot = 1*DotProduct(sigma_0=1)
rbf = 1*RBF(length_scale= 1)
mat = 1*Matern(length_scale= 2)
rat = 1*RationalQuadratic()
#Hyperparameters to test with
params = {'kernel': [dot, rbf, mat, rat]}

#Creates model 
gpc = GaussianProcessRegressor()
grid_search = GridSearchCV(estimator = gpc, param_grid = params, 
                          cv = 3, n_jobs = -1, verbose = 2)

grid_search.fit(x_train, y_train)
best = grid_search.best_params_
print(best)
