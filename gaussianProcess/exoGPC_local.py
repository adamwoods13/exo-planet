# -*- coding: utf-8 -*-
"""
Created on Sat Jan  2 14:35:22 2021

@author: Adam Woods
"""
import pandas as pd
from sklearn.gaussian_process import GaussianProcessClassifier
from sklearn.metrics import confusion_matrix
from sklearn import metrics
from sklearn.gaussian_process.kernels import RBF
from sklearn.gaussian_process.kernels import DotProduct
from sklearn.gaussian_process.kernels import Matern
from sklearn.gaussian_process.kernels import RationalQuadratic
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
from sklearn.metrics import roc_curve, auc

#Sets seed
import numpy as np
import random as rn
s = rn.randint(1,999)
np.random.seed(s)
rn.seed(s)

#loads data sets
df = pd.read_csv('../data/local_partial.csv', sep=',', header=None)

#Splits and shuffles the data into test and train
df = df.sample(frac=1).reset_index(drop=True)
df_train, df_test = train_test_split(df, test_size=0.1)

#Splits training and testing data into labels and features as well as other information
x_train = df_train.drop([0, 1, 2], axis=1)
y_train = df_train[2]
id_train = df_train[0]
label_train = df_train[1]
x_test = df_test.drop([0, 1, 2], axis=1)
y_test = df_test[2]
id_test = df_test[0]
label_test = df_test[1]


#Hyperparameters and constants
mat = 1*Matern(length_scale= 1)
params = {'kernel': mat}

#Creates model 
gpc = GaussianProcessClassifier(kernel=params['kernel'], n_jobs = -1)
gpc.fit(x_train, y_train.ravel())

#Generate a set of predictions for the test data
y_pred = gpc.predict(x_test)

#Shows accuracy for test set        
print('Accuracy: %.2f' % (metrics.accuracy_score(y_test, y_pred)*100))

#print out the confusion matrix and performance metrics
cm = confusion_matrix(y_test, y_pred, [1, 0])
perf = metrics.classification_report(y_test, y_pred)
print("Seed: ", s)
print("Confusion Matrix:")
print(cm)
print("Classification Report:")
print(perf)

#Generrates a probability score for the test data and plots AUC_ROC curve
y_prob = gpc.predict_proba(x_test)[:, 1]
fpr, tpr, thresholds = roc_curve(y_test, y_prob)
area = auc(fpr, tpr)
plt.figure(1)
plt.plot([0, 1], [0, 1], 'k--')
plt.plot(fpr, tpr, label='GPC (area = {:.3f})'.format(area))
plt.xlabel('False positive rate')
plt.ylabel('True positive rate')
plt.title('ROC curve')
plt.legend(loc='best')
plt.show()