# -*- coding: utf-8 -*-
"""
Created on Sun Feb 21 13:36:57 2021

@author: adamw
"""

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import statistics as s

#loads data sets
df = pd.read_csv('data/local_data.csv', sep=',', header=None)

#Splits all data into labels and features as well as other information
x_train = df.drop([0, 1, 2], axis=1)
y_train = df[2]
id_train = df[0]
label_train = df[1]

deviation = []
mean_flux = []
max_list = []
min_list = []
for index, row in x_train.iterrows():
    avg = sum(row) / len(row)
    dev = s.stdev(row)
    deviation.append(dev)
    mean_flux.append(avg)
    minimum = min(row)
    maximum = max(row)
    max_list.append(maximum)
    min_list.append(minimum)

s = [i *5 for i in max_list]
plt.figure(figsize=(100,100))
plt.scatter(mean_flux, deviation, s=s, c=y_train)
plt.title('Nonlinear data')
plt.xlabel('Mean Flux')
plt.ylabel('Deviation')
plt.savefig('myimage.svg', format='svg', dpi=1200)
plt.show()