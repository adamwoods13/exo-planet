# -*- coding: utf-8 -*-
"""
Created on Tue Dec 29 16:31:09 2020

@author: Adam Woods
"""
import pandas as pd

"""
Removes UNK values from the labels as this is not useful for training models.
"""

#Reads csv file for kepler id and labels
df=pd.read_csv('data/dr24_labels.csv', sep=',', header=None)
df.columns = ["kepId", "period", "t0", "duration", "label"]

#removes rows including UNK in the labael column
df = df[~df['label'].isin(['UNK'])]

#Outputs to CSV
df.to_csv('data/dr24_labels.csv', sep=',', header=None)