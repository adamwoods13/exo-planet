SVM
LOCAL
#1	93.01
#2	91.22
#3	91.96
#4	92.11
#5	90.92
#6	90.33
#7	91.52
#8	89.58
#9	91.67
#10	90.18

BEST: 93.01
WORST: 89.58
AVERAGE: 91.85
AUC_ROC: 96.00

GLOBAL
#1	90.92
#2 	91.52
#3	90.18
#4	89.88	961
#5	90.48	958
#6	90.03	960
#7	88.69	957	
#8	89.73	955
#9	90.18	957
#10	89.43	960

BEST: 91.52
WORST: 88.69
AVERAGE: 90.10
AUC_ROC: 95.83

GLOBAL AND LOCAL
#1	
#2 
#3
#4
#5
#6
#7
#8
#9
#10

BEST:
WORST:
AVERAGE:
