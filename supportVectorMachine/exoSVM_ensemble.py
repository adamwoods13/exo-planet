# -*- coding: utf-8 -*-
"""
Created on Tue Feb 23 12:49:08 2021

@author: Adam Woods
"""
import pandas as pd
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import confusion_matrix
from sklearn import metrics
from sklearn.tree import export_graphviz
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
from sklearn.metrics import roc_curve, auc
import functools 

#Sets seed
import numpy as np
import random as rn
s = rn.randint(1,999) #407 93.60
np.random.seed(s)
rn.seed(s)

#loads data sets
df = pd.read_csv('../data/local_data.csv', sep=',', header=None)

#Splits and shuffles the data into test and train
df = df.sample(frac=1).reset_index(drop=True)
df_train, df_test = train_test_split(df, test_size=0.1)

#Splits training and testing data into labels and features as well as other information
x_train = df_train.drop([0, 1, 2], axis=1)
y_train = df_train[2]
id_train = df_train[0]
label_train = df_train[1]
x_test = df_test.drop([0, 1, 2], axis=1)
y_test = df_test[2]
id_test = df_test[0]
label_test = df_test[1]

def generate_rf(x_train, y_train, x_test, y_test):
    rf = RandomForestClassifier(n_estimators=100, min_samples_leaf=12, min_samples_split=4)
    rf.fit(x_train, y_train)
    print("rf score ", rf.score(x_test, y_test))
    return rf

def combine_rfs(rf_a, rf_b):
    rf_a.estimators_ += rf_b.estimators_
    rf_a.n_estimators = len(rf_a.estimators_)
    return rf_a

# Create 10 RFs
local_rfs = [generate_rf(x_train, y_train, x_test, y_test) for i in range(10)]
rf_combined = functools.reduce(combine_rfs, local_rfs)


#Predicts for test set
y_pred=rf_combined.predict(x_test)
print('Accuracy: %.2f' % (metrics.accuracy_score(y_test, y_pred)*100))

#selects a single binary tree
estimator = rf_combined.estimators_[5]

# Export tree as dot file
export_graphviz(estimator, 
                out_file='tree.dot', 
                class_names = ["0","1"],
                rounded = True, proportion = False, 
                precision = 2,
                filled = True)

#print out the confusion matrix and performance metrics
cm = confusion_matrix(y_test, y_pred, [1, 0])
perf = metrics.classification_report(y_test, y_pred)
print("Seed: ", s)
print("Confusion Matrix:")
print(cm)
print("Classification Report:")
print(perf)

#Generrates a probability score for the test data and plots AUC_ROC curve
y_prob = rf_combined.predict_proba(x_test)[:, 1]
fpr, tpr, thresholds = roc_curve(y_test, y_prob)
area = auc(fpr, tpr)
plt.figure(1)
plt.plot([0, 1], [0, 1], 'k--')
plt.plot(fpr, tpr, label='RF (area = {:.3f})'.format(area))
plt.xlabel('False positive rate')
plt.ylabel('True positive rate')
plt.title('ROC curve')
plt.legend(loc='best')
plt.show()