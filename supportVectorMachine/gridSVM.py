# -*- coding: utf-8 -*-
"""
Created on Thu Jan 21 14:21:08 2021

@author: Adam Woods
"""
import pandas as pd
from sklearn import svm
from sklearn.model_selection import train_test_split
from sklearn.model_selection import GridSearchCV

#loads data sets
df = pd.read_csv('../data/global_data.csv', sep=',', header=None)

#Splits and shuffles the data into test and train
df = df.sample(frac=1).reset_index(drop=True)
df_train, df_test = train_test_split(df, test_size=0.1)

#Splits training and testing data into labels and features as well as other information
x_train = df_train.drop([0, 1, 2], axis=1)
y_train = df_train[2]
id_train = df_train[0]
label_train = df_train[1]
x_test = df_test.drop([0, 1, 2], axis=1)
y_test = df_test[2]
id_test = df_test[0]
label_test = df_test[1]

#Hyperparameters to test with
params = {'kernel': ['poly', 'rbf', 'sigmoid'],
           'degree': [1, 3],
           'gamma': ['scale', 'auto'],
           'coef0': [0.0, 0.2],
           'tol': [1e-3],
           'probability': [True]}

# Creates SVM using Radial Basis Function Kernel
svmc = svm.SVC()
grid_search = GridSearchCV(estimator = svmc, param_grid = params, 
                          cv = 3, n_jobs = -1, verbose = 2)
grid_search.fit(x_train, y_train)
best = grid_search.best_params_
print(best)