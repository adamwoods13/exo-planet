# -*- coding: utf-8 -*-
"""
Created on Thu Feb  4 16:29:09 2021

@author: Adam Woods
"""
import pandas as pd
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import confusion_matrix
from sklearn import metrics
from sklearn.tree import export_graphviz
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
from sklearn.metrics import roc_curve, auc


#Sets seed
import numpy as np
import random as rn
s = rn.randint(1,999) #949 93.75
np.random.seed(s)
rn.seed(s)


#loads local and global data sets
local_df = pd.read_csv('../data/local_data.csv', sep=',', header=None)
global_df = pd.read_csv('../data/global_data.csv', sep=',', header=None)

#Splits training and testing data into labels and features as well as other information
l_x = local_df.drop([0, 1, 2], axis=1)
l_y = local_df[2]
l_id = local_df[0]
l_label = local_df[1]
g_x = global_df.drop([0, 1, 2], axis=1)
g_y = global_df[2]
g_id = global_df[0]
g_label = global_df[1]

#Concatenates data for global and local
gl = pd.concat([g_x, l_x], axis=1)
df = pd.concat([g_y, gl], axis=1)
#Splits and shuffles the data into test and train
train, test = train_test_split(df, test_size=0.1)

#Splits into features and labels
x_train = train.drop([2], axis=1)
y_train = train[2]
x_test = test.drop([2], axis=1)
y_test = test[2]


#Hyperparameters and constants
params = {'n_estimators': 200,
           'min_samples_split': 3,
           'min_samples_leaf': 14,
           'bootstrap': True}

#Creates model with 100 binary trees
rf=RandomForestClassifier(n_estimators=params['n_estimators'], 
                          min_samples_split=params['min_samples_split'], 
                          min_samples_leaf=params['min_samples_leaf'])
rf.fit(x_train, y_train)

#Predicts for test set
y_pred=rf.predict(x_test)
print('Accuracy: %.2f' % (metrics.accuracy_score(y_test, y_pred)*100))

#selects a single binary tree
estimator = rf.estimators_[5]

# Export tree as dot file
export_graphviz(estimator, 
                out_file='tree.dot', 
                class_names = ["0","1"],
                rounded = True, proportion = False, 
                precision = 2,
                filled = True)

#print out the confusion matrix and performance metrics
cm = confusion_matrix(y_test, y_pred, [1, 0])
perf = metrics.classification_report(y_test, y_pred)
print("Seed: ", s)
print("Confusion Matrix:")
print(cm)
print("Classification Report:")
print(perf)

#Generrates a probability score for the test data and plots AUC_ROC curve
y_prob = rf.predict_proba(x_test)[:, 1]
fpr, tpr, thresholds = roc_curve(y_test, y_prob)
area = auc(fpr, tpr)
plt.figure(1)
plt.plot([0, 1], [0, 1], 'k--')
plt.plot(fpr, tpr, label='RF (area = {:.3f})'.format(area))
plt.xlabel('False positive rate')
plt.ylabel('True positive rate')
plt.title('ROC curve')
plt.legend(loc='best')
plt.show()