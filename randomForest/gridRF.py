# -*- coding: utf-8 -*-
"""
Created on Fri Jan 1 14:13:21 2021

@author: Adam Woods
"""
import pandas as pd
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.model_selection import GridSearchCV

#loads data sets
df = pd.read_csv('../data/global_data.csv', sep=',', header=None)

#Splits and shuffles the data into test and train
df = df.sample(frac=1).reset_index(drop=True)
df_train, df_test = train_test_split(df, test_size=0.1)

#Splits training and testing data into labels and features as well as other information
x_train = df_train.drop([0, 1, 2], axis=1)
y_train = df_train[2]
id_train = df_train[0]
label_train = df_train[1]
x_test = df_test.drop([0, 1, 2], axis=1)
y_test = df_test[2]
id_test = df_test[0]
label_test = df_test[1]

#Hyperparameters to test with
params = {'n_estimators': [50, 100, 200],
           'max_depth': [50, 80, 100],
           'min_samples_split': [3, 5, 7],
           'min_samples_leaf': [5, 8, 12],
           'bootstrap': [True]}

#Creates model
rf=RandomForestClassifier()
grid_search = GridSearchCV(estimator = rf, param_grid = params, 
                          cv = 3, n_jobs = -1, verbose = 2)
grid_search.fit(x_train, y_train)
#Gives best parameters
best = grid_search.best_params_
print(best)