# -*- coding: utf-8 -*-
"""
Created on Wed Dec 16 15:24:40 2020

@author: Adam Woods
"""
import lightkurve as lk
import numpy as np
import pandas as pd

df=pd.read_csv('data/lc_labels11.csv', sep=',', header=None)
df.columns = ["kepId", "period", "t0", "duration", "label"]

def get_lightcurve(targetNum, period, t0, duration):
    target = "KIC " + str(targetNum)
    
    #Downloads light curve files for all quarters for a target
    lc_collection = lk.search_lightcurvefile(target, mission='Kepler').download_all()
    print(target)
    print(period, t0, duration)
    
    #Stitches the quarters together into one light curve
    stitched_lc = lc_collection.PDCSAP_FLUX.stitch()
    
    #Removes outliers such as those caused by solar flares
    lc_clean = stitched_lc.remove_outliers(sigma=20, sigma_upper=4)
    
    #folds the light curve and masks the transit to avoid issues when flattening
    temp_fold = lc_clean.fold(period, t0=t0)
    fractional_duration = (duration / 24.0) / period
    phase_mask = np.abs(temp_fold.phase) < (fractional_duration * 1.5)
    transit_mask = np.in1d(lc_clean.time, temp_fold.time_original[phase_mask])
    
    #flattens the light curve over the signal of interest
    lc_flat, trend_lc = lc_clean.flatten(return_trend=True, mask=transit_mask)
    lc_fold = lc_flat.fold(period, t0=t0)
    
    #Creates a global view with 2001 data points
    lc_global = lc_fold.bin(bins=2000, method='median') - 1
    lc_global = (lc_global / np.abs(lc_global.flux.min()) ) * 2.0 + 1
    
    #Creates a dataframe of the global view
    df_global = lc_global.to_pandas()
    df_global_flux = pd.DataFrame(df_global['flux']).T
    
    #Creates a local view with 201 data points
    phase_mask = (lc_fold.phase > -4*fractional_duration) & (lc_fold.phase < 4.0*fractional_duration)
    lc_zoom = lc_fold[phase_mask]
    
    lc_local = lc_zoom.bin(bins=200, method='median') - 1
    lc_local = (lc_local / np.abs(lc_local.flux.min()) ) * 2.0 + 1
    
    #Creates a dataframe of the local view
    df_local = lc_local.to_pandas()
    df_local_flux = pd.DataFrame(df_local['flux']).T
    #Exports the data to a csv file
    
    return df_global_flux, df_local_flux

#get_lightcurve(targetNum, period, t0, duration)
global_collection = pd.DataFrame()
local_collection = pd.DataFrame()

#Iterates through all Kepler IDs
for index, row in df.iterrows():
    #try to catch multiple targets returned issue.
    try:
        print("\n", index)
        df_global_flux, df_local_flux = get_lightcurve(row['kepId'], row['period'], row['t0'], row['duration'])
        #checks if local and global view have been made successfully
        if df_global_flux.isnull().values.any() or df_local_flux.isnull().values.any():
            #removes from labels if a view cannot be generated
            print("Could not create local/global view.")
            df.drop(index, inplace=True)
        else:
            global_collection = global_collection.append(df_global_flux)
            local_collection = local_collection.append(df_local_flux)
    except ConnectionError:
        print("Connetion issue, skipping:", row['kepId'], "\n")
        df.drop(index, inplace=True)
    except ValueError:
        print("Multiple targets found, skipping data.", "\n")
        df.drop(index, inplace=True)
    except AttributeError:
        print("No target found, skipping data.", "\n")
        df.drop(index, inplace=True)
    except FileNotFoundError:
        print("File error", "\n")
        df.drop(index, inplace=True)
        
df.to_csv('data/lc_labels_filtered11.csv', sep=',', header=None, index=False)
global_collection.to_csv('data/dr24_features_global11.csv', sep=',', header=None, index=False)
local_collection.to_csv('data/dr24_features_local11.csv', sep=',', header=None, index=False)  
