# -*- coding: utf-8 -*-
"""
Created on Mon Feb  1 14:00:44 2021

@author: Adam Woods
"""
import pandas as pd
from keras.models import Sequential
from keras.layers import Dense, Flatten, Conv1D, Dropout
from sklearn.model_selection import train_test_split
from sklearn.model_selection import GridSearchCV
from keras.wrappers.scikit_learn import KerasClassifier

#loads data sets
df = pd.read_csv('../data/local_data.csv', sep=',', header=None)

#Splits and shuffles the data into train, test and validation
df = df.sample(frac=1).reset_index(drop=True)
df_train, df_test = train_test_split(df, test_size=0.1)
df_train, df_val = train_test_split(df_train, test_size=0.1)

#Splits all data into labels and features as well as other information
x_train = df_train.drop([0, 1, 2], axis=1)
y_train = df_train[2]
id_train = df_train[0]
label_train = df_train[1]
x_test = df_test.drop([0, 1, 2], axis=1)
y_test = df_test[2]
id_test = df_test[0]
label_test = df_test[1]
x_val = df_val.drop([0, 1, 2], axis=1)
y_val = df_val[2]
id_val = df_val[0]
label_val = df_val[1]


#Converts pandas dataframe to float32 and reshapes each sample into a vector of LOCAL: 40:5/20:10 - GLOBAL: 40:50/80:25
x_train = x_train.to_numpy()
x_test = x_test.to_numpy()
x_val = x_val.to_numpy()
x_train = x_train.reshape(x_train.shape[0], 40, 5).astype('float32')
x_test = x_test.reshape(x_test.shape[0], 40, 5).astype('float32')
x_val = x_val.reshape(x_val.shape[0], 40, 5).astype('float32')

#Hyperparameters and constants
params = {'batch_size':[16, 32, 64],
          'epochs':[20, 50, 100]}

#Create model
def create_model():
    model = Sequential()
    model.add(Conv1D(filters=128, kernel_size=6, input_shape=x_train[1].shape, activation='relu'))
    model.add(Conv1D(filters=64, kernel_size=4, activation='relu'))
    model.add(Flatten())
    model.add(Dropout(0.2))
    model.add(Dense(32, input_dim=50, activation='relu'))
    model.add(Dropout(0.2))
    model.add(Dense(16, input_dim=50, activation='relu'))
    model.add(Dropout(0.2))
    model.add(Dense(8, activation='relu'))
    model.add(Dense(1, activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
    return model


model = KerasClassifier(build_fn=create_model(), verbose=0)

grid_search = GridSearchCV(estimator=model, param_grid=params, n_jobs=-1, cv=3)
grid_result = grid_search.fit(x_train, y_train)

best = grid_search.best_params_
print(best)