# -*- coding: utf-8 -*-
"""
Created on Mon Feb  8 13:16:23 2021

@author: Adam Woods
"""
import pandas as pd
from keras import Model
from keras.layers import Dense, Flatten, Conv1D, Dropout, LeakyReLU, Input, MaxPooling1D
from keras.callbacks import ModelCheckpoint, EarlyStopping
from keras.layers.merge import Average, Concatenate, Multiply
from sklearn.model_selection import train_test_split
from sklearn import metrics
import matplotlib.pyplot as plt
from sklearn.metrics import roc_curve, auc
from keras.models import load_model
from keras.regularizers import l1

#Sets seed
import numpy as np
import random as rn
s = 244 #rn.randint(1,999)
np.random.seed(s)
rn.seed(s)


#loads data sets
local_df = pd.read_csv('../data/local_data.csv', sep=',', header=None)

#Splits and shuffles the data into train, test and validation
local_df = local_df.sample(frac=1).reset_index(drop=True)
local_df_train, local_df_test = train_test_split(local_df, test_size=0.1, shuffle=False)
local_df_train, local_df_val = train_test_split(local_df_train, test_size=0.1, shuffle=False)

#Splits all data into labels and features as well as other information
local_x_train = local_df_train.drop([0, 1, 2], axis=1)
local_y_train = local_df_train[2]
local_id_train = local_df_train[0]
local_label_train = local_df_train[1]
local_x_test = local_df_test.drop([0, 1, 2], axis=1)
local_y_test = local_df_test[2]
local_id_test = local_df_test[0]
local_label_test = local_df_test[1]
local_x_val = local_df_val.drop([0, 1, 2], axis=1)
local_y_val = local_df_val[2]
local_id_val = local_df_val[0]
local_label_val = local_df_val[1]

#Converts to Numpy array and reshapes for input into Conv1D
local_x_train = local_x_train.to_numpy()
local_x_test = local_x_test.to_numpy()
local_x_val = local_x_val.to_numpy()
local_x_train = local_x_train.reshape(local_x_train.shape[0], 200, 1).astype('float32')
local_x_test = local_x_test.reshape(local_x_test.shape[0], 200, 1).astype('float32')
local_x_val = local_x_val.reshape(local_x_val.shape[0], 200, 1).astype('float32')

global_df = pd.read_csv('../data/global_data.csv', sep=',', header=None)

#Splits and shuffles the data into train, test and validation
global_df = global_df.sample(frac=1).reset_index(drop=True)
global_df_train, global_df_test = train_test_split(global_df, test_size=0.1, shuffle=False)
global_df_train, global_df_val = train_test_split(global_df_train, test_size=0.1, shuffle=False)

#Splits all data into labels and features as well as other information
global_x_train = global_df_train.drop([0, 1, 2], axis=1)
global_y_train = global_df_train[2]
global_id_train = global_df_train[0]
global_label_train = global_df_train[1]
global_x_test = global_df_test.drop([0, 1, 2], axis=1)
global_y_test = global_df_test[2]
global_id_test = global_df_test[0]
global_label_test = global_df_test[1]
global_x_val = global_df_val.drop([0, 1, 2], axis=1)
global_y_val = global_df_val[2]
global_id_val = global_df_val[0]
global_label_val = global_df_val[1]

#Converts to Numpy array and reshapes for input into Conv1D
global_x_train = global_x_train.to_numpy()
global_x_test = global_x_test.to_numpy()
global_x_val = global_x_val.to_numpy()
global_x_train = global_x_train.reshape(global_x_train.shape[0], 2000, 1).astype('float32')
global_x_test = global_x_test.reshape(global_x_test.shape[0], 2000, 1).astype('float32')
global_x_val = global_x_val.reshape(global_x_val.shape[0], 2000, 1).astype('float32')

#Hyperparameters and constants
local_params = {'kernel':4,
                'input_shape':local_x_train[1].shape,
                'activation':'relu'}

global_params = {'kernel':4s0,
                 'input_shape':global_x_train[1].shape,
                 'activation':'relu'}
                 
merged_params = {'dropout':0.5,
                 'activation':'relu',
                 'last_activation':'sigmoid'}

params = {'batch_size':32,
          'epochs':200,
          'losses': 'binary_crossentropy',
          'optimizer':'adam'}

#Create local model
local_input = Input(shape=local_x_train[1].shape)
local_model = Conv1D(filters=32, kernel_size=local_params['kernel'], input_shape=local_params['input_shape'], activation=local_params['activation'])(local_input)
local_model = MaxPooling1D(pool_size=2, strides=2)(local_model)
local_model = Flatten()(local_model)
local_model = Dense(32, activation=local_params['activation'])(local_model)


#Create global model
global_input = Input(shape=global_x_train[1].shape)
global_model = Conv1D(filters=16, kernel_size=global_params['kernel'], input_shape=global_params['input_shape'], activation=global_params['activation'])(global_input)
global_model = MaxPooling1D(pool_size=2, strides=2)(global_model)
global_model = Flatten()(global_model)
global_model = Dense(16, activation=global_params['activation'])(global_model)

#Create merged model using concatenation of Dense
merg = Concatenate()([local_model, global_model])
merged_model = Dense(8, activation=merged_params['activation'], kernel_regularizer=l1(1))(merg)
merged_model = Dropout(merged_params['dropout'])(merged_model)
out = Dense(1, activation=merged_params['last_activation'])(merged_model)

model = Model(inputs=[local_input, global_input], outputs=out)

model.compile(optimizer = params['optimizer'], loss = params['losses'],  metrics = ['accuracy'])

#Gives summary of data inputs and outputs for documentation
model.summary()

#Earlystopping based on lowest validation loss if no increase is seen after 20 epochs
cb = [EarlyStopping(monitor='val_loss', patience=10),
             ModelCheckpoint(filepath='best_model.h5', monitor='val_loss', save_best_only=True)]

#fits data
history = model.fit([local_x_train, global_x_train], local_y_train,
                    epochs=params['epochs'], 
                    batch_size=params['batch_size'],
                    validation_data=([local_x_val, global_x_val], local_y_val), 
                    shuffle=True,
                    callbacks=cb)

#loads the best model saved by the call back.
saved_model = load_model('best_model.h5')

#Calculates accuracy of the model on the test data
_, accuracy = saved_model.evaluate([local_x_test, global_x_test], local_y_test)
print('Accuracy: %.2f' % (accuracy*100))

#Generates boolean predictions
y_prob = saved_model.predict([local_x_test, global_x_test])
y_pred = (y_prob > 0.5)

#Creates confusion matrix and performance metrics
cm = metrics.confusion_matrix(local_y_test, y_pred)
perf = metrics.classification_report(local_y_test, y_pred)
print("Seed: ", s)
print("Confusion Matrix:")
print(cm)
print("Classification Report:")
print(perf)

#Plots accuracy
plt.plot(history.history['acc'])
plt.plot(history.history['val_acc'])
plt.title('model accuracy')
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['train', 'validation'], loc='upper left')
plt.show()

#Plots loss
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('model loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train', 'validation'], loc='upper left')
plt.show()

#Plots AUC_ROC curve
fpr, tpr, thresholds_rf = roc_curve(local_y_test, y_prob)
area = auc(fpr, tpr)
plt.figure(1)
plt.plot([0, 1], [0, 1], 'k--')
plt.plot(fpr, tpr, label='CNN (area = {:.4f})'.format(area))
plt.xlabel('False positive rate')
plt.ylabel('True positive rate')
plt.title('ROC curve')
plt.legend(loc='best')
plt.show()
