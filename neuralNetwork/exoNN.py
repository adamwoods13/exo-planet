# -*- coding: utf-8 -*-
"""
Created on Fri Jan 22 13:08:11 2021

@author: adamw
"""
import pandas as pd
from keras.models import Sequential
from keras.layers import Dense
from sklearn.model_selection import train_test_split
from sklearn import metrics


#loads data sets
df = pd.read_csv('../data/local_data.csv', sep=',', header=None)

#Splits and shuffles the data into test and train
df = df.sample(frac=1).reset_index(drop=True)
df_train, df_test = train_test_split(df, test_size=0.1)

#Splits training and testing data into labels and features as well as other information
x_train = df_train.drop([0, 1, 2, 3], axis=1)
y_train = df_train[2]
id_train = df_train[0]
label_train = df_train[1]
x_test = df_test.drop([0, 1, 2, 3], axis=1)
y_test = df_test[2]
id_test = df_test[0]
label_test = df_test[1]


#Create model
model = Sequential()
model.add(Dense(128, input_dim=200, activation='relu'))
model.add(Dense(64, activation='relu'))
model.add(Dense(32, activation='relu'))
model.add(Dense(16, activation='relu'))
model.add(Dense(8, activation='relu'))
model.add(Dense(1, activation='sigmoid'))

model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])

model.fit(x_train, y_train, epochs=50, batch_size=32)
predictions = model.predict_proba(x_test)

#Calculates accuracy of the model on the test data
_, accuracy = model.evaluate(x_test, y_test)
print('Accuracy: %.2f' % (accuracy*100))

#Generates boolean predictions
y_pred = (predictions > 0.5)

#Creates confusion matrix and performance metrics
cm = metrics.confusion_matrix(y_test, y_pred)
perf = metrics.classification_report(y_test, y_pred)
print("Confusion Matrix:")
print(cm)
print("Classification Report:")
print(perf)