# -*- coding: utf-8 -*-
"""
Created on Tue Jan 19 13:53:11 2021

@author: Adam Woods
"""
import pandas as pd
from keras import Model
from keras.layers import Dense, Flatten, Conv1D, Dropout, LeakyReLU, MaxPooling1D, Input
from keras.callbacks import ModelCheckpoint, EarlyStopping
from sklearn.model_selection import train_test_split
from sklearn import metrics
import matplotlib.pyplot as plt
from sklearn.metrics import roc_curve, auc
from keras.models import load_model
from keras.regularizers import l1

#Sets seed
import numpy as np
import random as rn
s = 969#rn.randint(1,999)
np.random.seed(s)
rn.seed(s)

#loads data sets
df = pd.read_csv('../data/local_partial.csv', sep=',', header=None)

#Splits and shuffles the data into train, test and validation
df = df.sample(frac=1).reset_index(drop=True)
df_train, df_test = train_test_split(df, test_size=0.1)
df_train, df_val = train_test_split(df_train, test_size=0.1)

#Splits all data into labels and features as well as other information
x_train = df_train.drop([0, 1, 2], axis=1)
y_train = df_train[2]
id_train = df_train[0]
label_train = df_train[1]
x_test = df_test.drop([0, 1, 2], axis=1)
y_test = df_test[2]
id_test = df_test[0]
label_test = df_test[1]
x_val = df_val.drop([0, 1, 2], axis=1)
y_val = df_val[2]
id_val = df_val[0]
label_val = df_val[1]


#Converts to Numpy array for input into Conv1D
x_train = x_train.to_numpy()
x_test = x_test.to_numpy()
x_val = x_val.to_numpy()
x_train = x_train.reshape(x_train.shape[0], 200, 1).astype('float32')
x_test = x_test.reshape(x_test.shape[0], 200, 1).astype('float32')
x_val = x_val.reshape(x_val.shape[0], 200, 1).astype('float32')

#Hyperparameters and constants
params = {'kernel':4, #10
          'batch_size':32,
          'epochs':100,
          'dropout':0.5,
          'input_shape':x_train[1].shape,
          'optimizer':'adam',
          'losses': 'binary_crossentropy',
          'activation':'relu',
          'last_activation':'sigmoid'}

#Create local model
local_input = Input(shape=x_train[1].shape)
local_model = Conv1D(filters=32, kernel_size=params['kernel'], input_shape=params['input_shape'], activation=params['activation'])(local_input)
local_model = MaxPooling1D(pool_size=2, strides=2)(local_model)
local_model = Flatten()(local_model)
local_model = Dense(32, activation=params['activation'])(local_model)
local_model = Dense(8, activation=params['activation'], kernel_regularizer=l1(1))(local_model)
local_model = Dropout(params['dropout'])(local_model)
out = Dense(1, activation=params['last_activation'])(local_model)

model = Model(inputs=local_input, outputs=out)

model.compile(optimizer = params['optimizer'], loss = params['losses'],  metrics = ['accuracy'])

#Earlystopping based on lowest validation loss if no increase is seen after 20 epochs
cb = [EarlyStopping(monitor='val_loss', patience=10),
             ModelCheckpoint(filepath='best_model.h5', monitor='val_loss', save_best_only=True)]

history = model.fit(x_train, y_train,
                    epochs=params['epochs'], 
                    batch_size=params['batch_size'],
                    validation_data=(x_val, y_val),
                    callbacks=cb)

#Gives summary of data inputs and outputs for documentation
model.summary()

#loads the best model saved by the call back.
saved_model = load_model('best_model.h5')

#Calculates accuracy of the model on the test data
_, accuracy = saved_model.evaluate(x_test, y_test)
print('Accuracy: %.2f' % (accuracy*100))

#Generates boolean predictions
y_prob = saved_model.predict(x_test)
y_pred = (y_prob > 0.5)

#Creates confusion matrix and performance metrics
cm = metrics.confusion_matrix(y_test, y_pred)
perf = metrics.classification_report(y_test, y_pred)
print("Seed: ", s)
print("Confusion Matrix:")
print(cm)
print("Classification Report:")
print(perf)

#Plots accuracy
plt.plot(history.history['acc'])
plt.plot(history.history['val_acc'])
plt.title('model accuracy')
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['train', 'validation'], loc='upper left')
plt.show()

#Plots loss
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('model loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train', 'validation'], loc='upper left')
plt.show()

#Plots AUC_ROC curve

fpr, tpr, thresholds_rf = roc_curve(y_test, y_prob)
area = auc(fpr, tpr)
plt.figure(1)
plt.plot([0, 1], [0, 1], 'k--')
plt.plot(fpr, tpr, label='CNN (area = {:.3f})'.format(area))
plt.xlabel('False positive rate')
plt.ylabel('True positive rate')
plt.title('ROC curve')
plt.legend(loc='best')
plt.show()
