# -*- coding: utf-8 -*-
"""
Created on Wed Jan 13 13:27:08 2021

@author: Adam Woods
"""
import pandas as pd
#Reads csv file for kepler id and labels
df=pd.read_csv('data/dr24_labels.csv', sep=',', header=None)
df.columns = ["kepId", "period", "t0", "duration", "label"]

pc = df[df['label'].isin(['PC'])]
ntp = df[df['label'].isin(['NTP'])]
afp = df[df['label'].isin(['AFP'])]
afp = afp.reset_index()
afp = afp.drop(columns='index')


for i in range(7000):
    afp.drop(afp.index[[1]], inplace=True)


df = pd.concat([pc, ntp, afp])

df.to_csv('data/lc_labels.csv', sep=',', header=None)